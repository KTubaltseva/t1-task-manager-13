package ru.t1.ktubaltseva.tm.model;

import ru.t1.ktubaltseva.tm.enumerated.Status;

import java.util.UUID;

public final class Project {

    private final String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    public Project(final String name) {
        this.name = name;
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Project(final String name, final String description, final Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null)
            result += name;
        if (description != null)
            result += "\t(" + description + ")";
        if (status != null)
            result += "\t" + Status.toName(status) + "";
        return result;
    }

}
