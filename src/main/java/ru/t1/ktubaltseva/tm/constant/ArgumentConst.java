package ru.t1.ktubaltseva.tm.constant;

public class ArgumentConst {

    public static final String HELP = "-h";

    public static final String ABOUT = "-a";

    public static final String INFO = "-i";

    public static final String VERSION = "-v";

    private ArgumentConst() {
    }

}
