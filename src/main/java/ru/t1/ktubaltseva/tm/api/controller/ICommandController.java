package ru.t1.ktubaltseva.tm.api.controller;

public interface ICommandController {

    void displayAbout();

    void displayArgumentError();

    void displayCommandError();

    void displayHelp();

    void displaySystemInfo();

    void displayVersion();

    void exit();

}
