package ru.t1.ktubaltseva.tm.api.controller;

public interface ITaskController {

    void clearTasks();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void createTask();

    void displayTaskById();

    void displayTaskByIndex();

    void displayTasks();

    void displayTasksByProjectId();

    void displayTasksFullInfo();

    void removeTaskById();

    void removeTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}
