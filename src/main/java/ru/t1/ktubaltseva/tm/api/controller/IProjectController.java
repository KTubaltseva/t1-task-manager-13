package ru.t1.ktubaltseva.tm.api.controller;

public interface IProjectController {

    void clearProjects();

    void completeProjectById();

    void completeProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void createProject();

    void displayProjectById();

    void displayProjectByIndex();

    void displayProjects();

    void displayProjectsFullInfo();

    void removeProjectById();

    void removeProjectByIndex();
    void startProjectById();

    void startProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
