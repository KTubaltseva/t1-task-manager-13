package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject (String projectId, String taskId);

    Project removeProjectById (String projectId);

    Task unbindTaskFromProject (String projectId, String taskId);

}
